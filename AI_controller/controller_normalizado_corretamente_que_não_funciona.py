# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from __future__ import division
import numpy
from .state import State
import random
import datetime, time
import math
import sys
from random import randint
import operator

MAX_VARIATIONS_HILL = 100 #30 #QUANTIDADE DE "VIZINHOS" DO ESTADO ATUAL 
VARIATION_HILL = 2.0 #0.7 #VARIAÇÃO ALEATORIAS DOS VALORES DOS PARAMETROS
MAX_DEVIATION_SAMPLES = 20 #40 #AMOSTRAS DE PERFORMANCE DO MESMO PARAMETRO

class Controller:
    def __init__(self, game, load, state):
        self.initialize_parameters(game, load, state)
        self.initializeCommonVars()
        self.initializeVarHillClimb()
        #self.initializeVarSimulatedAnnealing()

    def initialize_parameters(self, game, load,state):
        self.state = state
        if load == None:
            self.parameters = numpy.random.uniform(-1.0, 1.0, 20)
        else:
            params = open(load, 'r')
            weights = params.read().split("\n")
            self.parameters = [float(x.strip()) for x in weights[0:]]
            print self.parameters

    def initializeCommonVars(self):
        self.bestParameters = 0
        self.bestPerformance = 0
        self.maxVel = 0
        self.endPer = 0
        self.maxSpeed = 0
        self.lowSpeed = 0

    def initializeVarHillClimb(self):
        self.listOfPerformance = list()
        self.listOfParameters  = list()
        self.listAverage = list()
        self.index = 0
        self.averageSamples = 0
        self.gerarParametros = True
     
    def initializeVarSimulatedAnnealing(self):
        self.Temperature = 1
        self.TemperatureMin = 0.1
        self.alpha = 0.5
        self.isDeviationGenerated = False
        


    def output(self, episode, performance):
       #print "Performance do episodio #%d: %d" % (episode, performance)
       if episode > 0 and episode % 10 == 0:
           output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
           for parameter in self.parameters:
               output.write(str(parameter) + "\n")
#--------------------------------------------------------------------------------------------------------

        #FUNCAO A SER COMPLETADA. Deve utilizar os pesos para calcular as funções de preferência Q para cada ação e retorna
    #-1 caso a ação desejada seja esquerda, +1 caso seja direita, e 0 caso seja ação nula
    def take_action(self, state):
        features = self.compute_features(state)
        #print "velocidade x:",state.velocity_x
        #print "velocidade y:",state.velocity_y 
        #stE = Θ0 + Θ1α.α' + Θ2x³ + Θ3w³ + Θ4velX.w² + Θ5w.velY + Θ6f    
        stE = ( self.parameters[0]
               +self.parameters[1]*features[0]
               +self.parameters[2]*features[1]
               +self.parameters[3]*features[2]
               +self.parameters[4]*features[3]
               +self.parameters[5]*features[4]
               +self.parameters[6]*features[5])
        #stD = Θ7 + Θ8α.α' + Θ9x³ + Θ10w³ + Θ11velX.w² + Θ12w.velY + Θ13f   
        stD = ( self.parameters[7]
               +self.parameters[8]*features[0]
               +self.parameters[9]*features[1]
               +self.parameters[10]*features[2]
               +self.parameters[11]*features[3]
               +self.parameters[12]*features[4]
               +self.parameters[13]*features[5])
        #st0 = Θ14 + Θ15α.α' + Θ16x³ + Θ17velX + Θ18f + Θ19.w
        st0 = ( self.parameters[14]
               +self.parameters[15]*features[0]
               +self.parameters[16]*features[1]
               +self.parameters[17]*features[6]
               +self.parameters[18]*features[7]
               +self.parameters[19]*features[8])
        #verifica se o movimento mais apropriado é para a esquerda
        #print "velocidade angular: ",state.angular_velocity
        if stE > stD and stE > st0:
            return -1
        #verifica se o movimento mais apropriado é para a direita
        if stD > stE and stD > st0:
            return 1

        return 0

    #FUNCAO A SER COMPLETADA. Deve calcular features expandidas do estados
    def compute_features(self,state):
        #featureNormalizada = 2*(featureOriginal+min)/(max-min) - 1
        features = numpy.random.uniform(0, 0, 9)
        features[0] = 2*(((state.rod_angle*state.angular_velocity)+212341.0)/(212341.0+212341.0))-1 
        features[1] = 2*(((state.wheel_x*state.wheel_x*state.wheel_x)+0.0)/(1728000000.0))-1
        features[2] = 2*(((state.wind*state.wind*state.wind)+125000000.0)/(125000000.0+125000000.0))-1
        features[3] = 2*((((state.wind*state.wind)*state.velocity_x)+635250000.0)/(654750000.0+635250000.0))-1 
        features[4] = 2*(((state.velocity_y*state.wind)+557000.0)/(557000.0+557000.0))-1
        features[5] = 2*((state.friction-0.97)/(0.999-0.97))-1
        features[6] = 2*((state.velocity_x+2541.0)/(2619.0+2541.0))-1
        features[7] = 2*((state.friction-0.97)/(0.999-0.97))-1
        features[8] = 2*((state.wind+500)/(500+500))-1

        for x in xrange(0,len(features)-1):
            if features[x] > 1.1 or features[x] < -1.1:
                print "feature #",x
                print "valor: ",features[x]
        return features
    
    #FUNCAO A SER COMPLETADA. Deve atualizar a propriedade self.parameters
    def update(self, episode, performance):
         #nossa versão do algoritmo Hill Climbing

        #TODO É preciso arrumar a geração de vizinhanças do hill climbing:
        #Gerar arrays de numeros aleatorios e somar com o parametro atual
        #fazer isso um numero x de vezes
        self.myHillClimbing(performance)
        #self.mySimulatedAnnealing(performance)
        return self.parameters
#------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------
#FUNÇÕES AUXILIARES NO PROCESSO DE APRENDIZADO DO CONTROLADOR ATRAVÉS DE HILL CLIMBING

    #função que vai gerar os desvios de cada componente theta    
    def generateParametersDeviationsHillClimbing(self):
        #insere os parametros ótimos na posição inicial da lista de parametros variados
        list_aux = list()
        list_aux.insert(0,self.parameters) 
       
        #gera parametros com variações aleatórias restringidas por self.variation em cada valor de theta
        #TODO Ao invéz de fazer um aleatorio para cada [index]+x e [index]-x
        #criar um array de aleatorios e somar aos parametros
        for x in range(0,MAX_VARIATIONS_HILL):
            aux1 = [random.uniform(-VARIATION_HILL,VARIATION_HILL) for _ in range(len(self.parameters))]
            list_aux.append(list(map(operator.add, aux1, self.parameters)))

        self.gerarParametros = False
        return list_aux #retorna lista com MAX_VARIATIONS_HILL vizinhos aleatorios do parametro atual

    #descobre quais são os melhores parametros entre os vizinhos gerados    
    def discoverBestParameters(self):
        maxPerformance = -1
        performanceIndex = 0
        for valor in range(0,len(self.listOfPerformance)-1):
            if maxPerformance < self.listOfPerformance[valor]:             	
                maxPerformance = self.listOfPerformance[valor]
                performanceIndex = valor

        if self.bestPerformance < self.listOfPerformance[performanceIndex]:
            self.bestParameters = self.listOfParameters[performanceIndex]
            self.bestPerformance = self.listOfPerformance[performanceIndex]

        print "PERFORMANCE DO MELHOR EPISODIO ESCOLHIDO: ",self.listOfPerformance[performanceIndex]
        self.endPer = self.listOfPerformance[performanceIndex]
        #print "PARAMETROS DO MELHOR EP: ",self.listOfParameters[performanceIndex]        
        return self.listOfParameters[performanceIndex]

    def myHillClimbing(self,performance):
        #se estivermos começando a avaliação dos possiveis estados, então: 
        if self.noDeviationsGenerated():
            self.listOfParameters = list(self.generateParametersDeviationsHillClimbing())#criar o array com as variações de self.parameters atual
            self.listOfPerformance = list()
            self.gerarParametros = False

        if not self.averagePerformanceDone():
            #print self.parameters
            self.listAverage.insert(self.averageSamples, performance)
        else:
            meanValue = float(sum(self.listAverage)/(len(self.listAverage)))
            self.listOfPerformance.insert(self.index, meanValue)#lista que recebe a função Valor(self.performance) do episodio anterior  
            self.parameters = list(self.listOfParameters[self.index+1])#alimenta os parametros com o próximo desvio calculado anteriormente 
            self.listAverage = list()
            #print "parametros : ",self.parameters
            #if meanValue >= self.bestPerformance:
                #print "performance media do parametro avaliado: ",meanValue
                #output = open("./finalPerformance/listOfPerformance.txt" , "a")
                #output.write(str(meanValue) + "\n")


        if self.endOfDeviationSimulation():
            best_parameters = self.discoverBestParameters()#fazer a escolha dos parametros que deram o maior Valor(s)
            
            if best_parameters is self.listOfParameters[0] or self.endPer == 20000:
                #continua com o valor anterior, pois não achamos nenhuma
                #configuração de parametros que de um valor maior na função Valor(s) usada anteriormente    
                self.endLearning(best_parameters) 
            else:
                #foi achado uma variação de parametros com Valor(s) maior que os parametros sem a variação
                self.parameters = list(best_parameters)
                self.gerarParametros = True
            self.index = 0 #reinicia o index      
        else:
            if self.averagePerformanceDone():
                self.index += 1
                self.averageSamples = 0
            else:
                self.averageSamples += 1
        

    #func aux que verifica se terminaram as simulações para as variações dos parametros originais
    def endOfDeviationSimulation(self):
        if self.index == (len(self.listOfParameters)-1):
            return True
        else:
            return False
    #func aux que verifica se foram geradas as variações para o parametro escolhido
    def noDeviationsGenerated(self):
        if self.gerarParametros:
            return True
        else:
            return False

    def endLearning(self,learntParams):
        output = open("./finalparams/learntParamsHC.txt" , "w+")
        for parameter in learntParams:
            output.write(str(parameter) + "\n")

        output = open("./finalparams/bestPerformanceParamsHC.txt" , "w+")
        for parameter in self.bestParameters:
            output.write(str(parameter) + "\n")  
        sys.exit(1)

    def averagePerformanceDone(self):
        if self.averageSamples == MAX_DEVIATION_SAMPLES:
            return True
        else:
            return False

    